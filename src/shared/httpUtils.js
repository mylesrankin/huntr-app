// Helper Functions
export function getSecureHeader(contentType) {
  if (!contentType) contentType = 'application/json';
  var headers = {
    'Content-Type': contentType,
  };

  return { ...headers, ...getAuthorisationHeader() };
}

export function getAuthorisationHeader() {
  const accessToken = localStorage.getItem('jwt_login_token');
  if (!accessToken) {
    return {};
  }
  return { Authorization: accessToken };
}
