import { combineReducers } from 'redux';
//Import reducers
import user from './ducks/user';

export default combineReducers({ user });
