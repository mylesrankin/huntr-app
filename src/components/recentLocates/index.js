import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { FixedSizeList } from 'react-window';

import moment from 'moment';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    height: 400,
    maxWidth: 300,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function RecentLocates() {
  const classes = useStyles();

  const dummyLocates = [
    { time: '2020-05-02T01:21:00+01:00', name: 'Joe Bloggs' },
    { time: '2020-05-01T01:21:00+01:00', name: 'Doe Bloggs' },
    { time: '2020-05-01T01:21:00+01:00', name: 'Poe Bloggs' },
    { time: '2020-05-01T01:01:00+01:00', name: 'Moe Bloggs' },
    { time: '2020-05-01T01:01:00+01:00', name: 'Chloe Bloggs' },
    { time: '2020-05-01T01:01:00+01:00', name: 'Flo Bloggs' },
    { time: '2020-05-01T01:01:00+01:00', name: 'Boe Bloggs' },
    { time: '2020-05-01T01:01:00+01:00', name: 'Woe Bloggs' },
    { time: '2020-05-01T01:01:00+01:00', name: 'Voe Bloggs' },
    { time: '2020-05-01T01:01:00+01:00', name: 'Noe Bloggs' },
    { time: '2020-05-01T01:01:00+01:00', name: 'Joe Bloggs' },
    { time: '2020-05-01T01:01:00+01:00', name: 'Joe Bloggs' },
  ];

  const renderRow = (props) => {
    const { index, style } = props;
    const value = dummyLocates[index];
    return (
      <ListItem dense button style={style} key={index}>
        <ListItemText
          m={0}
          primary={`${moment(value.time).startOf('hour').fromNow()} - ${
            value.name
          }`}
        />
      </ListItem>
    );
  };

  return (
    <div className={classes.root}>
      <FixedSizeList
        height={400}
        width={300}
        itemSize={20}
        itemCount={dummyLocates.length}
      >
        {renderRow}
      </FixedSizeList>
    </div>
  );
}
