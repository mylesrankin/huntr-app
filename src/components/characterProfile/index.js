import React, { useState, useEffect } from 'react';

import { makeStyles } from '@material-ui/core/styles';

import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import Avatar from '@material-ui/core/Avatar';

import fetch from 'node-fetch';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    //     textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  large: {
    width: theme.spacing(14),
    height: theme.spacing(14),
    margin: theme.spacing(2),
  },
}));

export default function CharacterProfile(props) {
  const classes = useStyles();
  const [profile, setProfile] = useState(<CircularProgress />);

  const loadPortrait = () => {
    return new Promise((resolve, reject) => {
      fetch(
        `https://esi.evetech.net/latest/search?categories=character&search=${props.characterId}`,
        { method: 'GET' }
      )
        .then((res) => res.json())
        .then((res) => {
          console.log(res);
          resolve(res);
        })
        .catch((err) => console.error(err));
    });
  };

  useEffect(() => {
    loadPortrait();
  }, [loadPortrait]);

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <Grid alignItems={'center'} container>
              <Grid item>
                <Avatar
                  src={`https://images.evetech.net/Character/${props.characterId}_128.jpg`}
                  className={classes.large}
                />
              </Grid>
              <Grid alignContent={'center'} item xs={4}>
                <Typography variant='h3'>ZehNarume</Typography>
                Eve-who - zKill
              </Grid>
            </Grid>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Paper className={classes.paper}>Locator History</Paper>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Paper className={classes.paper}>Comments</Paper>
        </Grid>
        <Grid item xs={6} sm={3}>
          <Paper className={classes.paper}>xs=6 sm=3</Paper>
        </Grid>
        <Grid item xs={6} sm={3}>
          <Paper className={classes.paper}>xs=6 sm=3</Paper>
        </Grid>
        <Grid item xs={6} sm={3}>
          <Paper className={classes.paper}>xs=6 sm=3</Paper>
        </Grid>
        <Grid item xs={6} sm={3}>
          <Paper className={classes.paper}>xs=6 sm=3</Paper>
        </Grid>
      </Grid>
    </div>
  );
}
