/* eslint-disable no-use-before-define */
import React, { useState, useRef, useEffect, useCallback } from 'react';
import fetch from 'node-fetch';

import { useHistory, useLocation } from 'react-router-dom';

import { makeStyles } from '@material-ui/core/styles';

import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';

import CharacterProfile from '../characterProfile';

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  control: {
    padding: theme.spacing(2),
  },
}));

export default function LookupSearch() {
  const classes = useStyles();
  const history = useHistory();
  const query = useQuery();
  const [characters, setCharacters] = useState([]);
  const [loading, setLoading] = useState(false);

  const [value, setValue] = useState(''); // STATE FOR THE INPUT VALUE
  const timeoutRef = useRef(null);

  const convertCharId = (characterId) =>
    new Promise((resolve, reject) => {
      fetch(`https://esi.evetech.net/latest/characters/${characterId}`, {
        method: 'GET',
      })
        .then((res) => res.json())
        .then((res) => resolve({ characterId: characterId, ...res }))
        .catch((err) => console.error(err));
    });

  const handleSearch = useCallback((value) => {
    fetch(
      `https://esi.evetech.net/latest/search?categories=character&search=${value}`,
      { method: 'GET' }
    )
      .then((res) => res.json())
      .then((res) => {
        console.log('got charids');
        console.log(res);
        console.log('grabbing char details');
        let chars = res.character.map((id) => convertCharId(id));
        console.log('finished map');
        return Promise.all(chars);
      })
      .then((res) => {
        setCharacters(res.sort((a, b) => a.name.localeCompare(b.name)));
        setLoading(false);
      })
      .catch((err) => console.error(err));
  }, []);

  const handleOnChange = (e, v, t) => {
    setValue(v);
  };

  const handleChange = (e, v, t) => {
    setValue(v);
    handleSubmit();
  };

  const handleSubmit = () => {
    let characterSubmit = characters.filter((c) => c.name === value);
    console.log(characterSubmit);
    history.push(`/lookup?characterId=${characterSubmit[0].characterId}`);
  };

  useEffect(() => {
    const timeout = setTimeout(() => {
      setCharacters([]);

      if (value !== '') {
        setLoading(true);
        handleSearch(value);
      }
    }, 1000);

    return () => {
      clearTimeout(timeout); // this guarantees to run right before the next effect
    };
  }, [value, handleSearch]);

  return (
    <Grid
      container
      className={classes.root}
      alignItems={'center'}
      justify={'center'}
      spacing={2}
    >
      <Grid item xs={6}>
        {/* <div style={{ width: 500 }}> */}
        <Autocomplete
          id='free-solo-demo'
          loading={loading}
          options={characters.map((option) => option.name)}
          onInputChange={handleOnChange}
          onChange={handleChange}
          renderInput={(params) => (
            <TextField
              {...params}
              label='Enter a character name '
              margin='normal'
              variant='outlined'
            />
          )}
        />
        {/* </div> */}
      </Grid>
      <Grid item xs={1}>
        <Button onClick={handleSubmit} variant='contained'>
          SEARCH
        </Button>
      </Grid>
      {query.get('characterId') ? (
        <Grid item xs={12}>
          <CharacterProfile
            characterId={query.get('characterId')}
          ></CharacterProfile>
        </Grid>
      ) : (
        ''
      )}
    </Grid>
  );
}
