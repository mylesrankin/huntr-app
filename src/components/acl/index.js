import React from 'react';
import { useSelector } from 'react-redux';

const checkPermissions = (user, acl) => {
  return user.some((r) => acl.indexOf(r) >= 0);
};

const Acl = ({ permissions, children }) => {
  const userPerms = useSelector((state) => state.user.permissions);
  return checkPermissions(userPerms, permissions) && children;
};

export default Acl;
