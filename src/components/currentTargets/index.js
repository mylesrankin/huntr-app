import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import StarIcon from '@material-ui/icons/Star';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function CurrentTargets() {
  const classes = useStyles();

  const dummyNames = [
    'Danilaw',
    'ZehNarume',
    'adopt',
    'simon perry',
    'Joe Bloggs',
    'Jonas Pepemaster',
    'RoCkEt X',
    'Jassmin Joy',
  ];

  return (
    <div>
      <List component='nav' className={classes.root} aria-label='contacts'>
        {dummyNames.map((name) => (
          <ListItem key={name} button>
            <ListItemIcon>
              <StarIcon />
            </ListItemIcon>
            <ListItemText primary={name} />
          </ListItem>
        ))}
        {/* <ListItem button>
        <ListItemIcon>
          <StarIcon />
        </ListItemIcon>
        <ListItemText primary='Chelsea Otakan' />
      </ListItem>
      <ListItem button>
        <ListItemText inset primary='Eric Hoffman' />
      </ListItem> */}
      </List>
    </div>
  );
}
