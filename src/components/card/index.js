import React from 'react';
import { useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';

export default function Card(props) {
  return (
    <React.Fragment>
      <Typography component='h2' variant='h6' color='primary' gutterBottom>
        {props.title}
      </Typography>
      <Paper
        elevation={0}
        variant={props.outlined && 'outlined'}
        style={{ maxHeight: 400, overflow: 'auto' }}
      >
        {props.children}
      </Paper>
    </React.Fragment>
  );
}
