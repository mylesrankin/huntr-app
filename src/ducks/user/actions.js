import types from './types';

const addUserToStore = (user) => ({ type: types.STORE_USER, payload: user });

const changeLoginStatus = (status) => ({
  type: types.UPDATE_LOGIN_STATUS,
  payload: status,
});

export default { addUserToStore, changeLoginStatus };
