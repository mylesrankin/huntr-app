import types from './types';

const INITIAL_STATE = {
  loginStatus: false,
};

const userReducer = (state = INITIAL_STATE, action) => {
  if (action == null) return state;
  switch (action.type) {
    //     case types.EXAMPLE:
    //       return {...state, anUpdate: 'test'};
    case types.STORE_USER:
      return { ...state, ...action.payload };
    default:
      return state;
  }
};

export default userReducer;
