import actions from './actions';
import { getSecureHeader } from '../../shared/httpUtils';

const updateLoginStatus = (status) => {
  return (dispatch) => {
    dispatch(actions.updateLoginStatus(status));
  };
};

const getUserDetails = () => {
  return (dispatch) => {
    console.log(getSecureHeader());
    fetch('http://localhost:3003/dev/api/user', {
      accept: 'application/json',
      headers: getSecureHeader(),
    })
      .then((res) => res.json())
      .then((userDetails) => {
        console.log(userDetails);
        dispatch(actions.addUserToStore(userDetails));
      })
      .catch((err) => console.error(err));
  };
};

export default { updateLoginStatus, getUserDetails };
