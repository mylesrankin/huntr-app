// Define types
// const STORE_USER = 'user/STORE_USER'

const STORE_USER = 'user/STORE_USER';

const UPDATE_LOGIN_STATUS = 'user/UPDATE_LOGIN_STATUS';

export default { STORE_USER, UPDATE_LOGIN_STATUS };
