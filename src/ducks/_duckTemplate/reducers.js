import types from './types';

const INITIAL_STATE = {};

const userReducer = (state = INITIAL_STATE, action) => {
  if (action == null) return state;
  switch (action.type) {
    //     case types.EXAMPLE:
    //       return {...state, anUpdate: 'test'};
    default:
      return state;
  }
};

export default userReducer;
