import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import moment from 'moment';

import logo from './logo.svg';
import './App.css';

import SsoCallback from './app/ssoCallback';
import Login from './app/login';
import Logout from './app/logout';

import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Link from '@material-ui/core/Link';

function Copyright() {
  return (
    <Typography variant='body2' color='textSecondary' align='center'>
      {'Mckay '}
      {new Date().getFullYear()}
    </Typography>
  );
}

function App() {
  const [session, setSession] = useState(
    localStorage.getItem('jwt_login_token')
  );

  useEffect(() => {
    localStorage.setItem('jwt_login_token', session);
    console.log(session);
  }, [session]);

  console.log(['undefined', null, false, 'null'].includes(session));
  return (
    <Container>
      <Box>
        <Router>
          {session}
          {['undefined', null, false, 'null'].includes(session) ? (
            <React.Fragment>
              <Route path='/sso-callback'>
                <SsoCallback setSession={setSession} />
              </Route>
              <Route path='/'>
                <Login></Login>
              </Route>
            </React.Fragment>
          ) : (
            <div>
              <nav>
                <ul>
                  <li>
                    <Link href='/' onClick={preventDefault}>
                      Home
                    </Link>
                  </li>
                  <li>
                    <Link href='/about' onClick={preventDefault}>
                      About
                    </Link>
                  </li>
                  <li>
                    <Link href='/users' onClick={preventDefault}>
                      Users
                    </Link>
                  </li>
                  <li>
                    <Link href='/logout' onClick={preventDefault}>
                      logout
                    </Link>
                  </li>
                </ul>
              </nav>

              {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
              <Switch>
                <Route path='/about'>
                  <About />
                </Route>
              </Switch>
            </div>
          )}
          <Route path='/logout'>
            <Logout setSession={setSession}></Logout>
          </Route>
        </Router>
        <Copyright />
      </Box>
    </Container>
  );
}

function Home() {
  return <h2>Home</h2>;
}

function About() {
  return <h2>About</h2>;
}

function Users() {
  return <h2>Users</h2>;
}

export default App;
