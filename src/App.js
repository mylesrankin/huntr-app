import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import { userOperations } from './ducks/user/index';

import SsoCallback from './app/ssoCallback';
import Login from './app/login';
import LoginButton from './app/loginButton';
import Logout from './app/logout';
import Acl from './components/acl';

// Material UI
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Box from '@material-ui/core/Box';

import Typography from '@material-ui/core/Typography';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

import Topbar from './app/template/topbar';
import Sidebar from './app/template/sidebar';

// Pages
import Dashboard from './app/pages/dashboard';
import Lookup from './app/pages/lookup';
import Structures from './app/pages/structures';

//import { mainListItems, secondaryListItems } from './listItems';
// import Chart from './Chart';
// import Deposits from './Deposits';
// import Orders from './Orders';

function Copyright() {
  return (
    <Typography variant='body2' color='textSecondary' align='center'>
      {'Mckay © '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },

  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },

  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
}));

const ContentPage = ({ title, children }) => {
  return (
    <React.Fragment>
      <Typography variant='h5' color='textSecondary'>
        {title}
      </Typography>
      {children}
    </React.Fragment>
  );
};

export default function App() {
  const dispatch = useDispatch();

  const user = useSelector((state) => state.user);

  const [session, setSession] = useState(
    localStorage.getItem('jwt_login_token')
  );
  const [open, setOpen] = React.useState(false);

  useEffect(() => {
    console.log(session);
    console.log(session === 'null');
    if (session !== 'null') {
      localStorage.setItem('jwt_login_token', session);
      dispatch(userOperations.getUserDetails());
      console.log(session);
    }
  }, [dispatch, session]);

  const classes = useStyles();

  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
  console.log(session);
  console.log(['undefined', null, false, 'null'].includes(session));
  if (
    ['undefined', null, false, 'null', ''].includes(session) ||
    typeof user._id === 'undefined'
  ) {
    return (
      <React.Fragment>
        <CssBaseline />
        <Router>
          <Switch>
            <Login>
              <Route path='/sso-callback'>
                <SsoCallback user={user} setSession={setSession} />
              </Route>
              <Route path='/'>
                <LoginButton></LoginButton>
              </Route>
            </Login>
          </Switch>
        </Router>
      </React.Fragment>
    );
  } else {
    return (
      <div className={classes.root}>
        <Router>
          <CssBaseline />
          <Topbar user={user} {...{ open, setOpen }}></Topbar>
          <Sidebar {...{ open, setOpen }}></Sidebar>
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth='lg' className={classes.container}>
              <Switch>
                <Route exact path='/'>
                  <ContentPage title='Dashboard'>
                    <Dashboard />
                  </ContentPage>
                </Route>
                <Route path='/test'>test</Route>

                <Route path='/lookup'>
                  <ContentPage title='Lookup'>
                    <Lookup />
                  </ContentPage>
                </Route>

                <Route path='/targets'>
                  <ContentPage title='Targets'>
                    <Dashboard />
                  </ContentPage>
                </Route>

                <Route path='/statistics'>
                  <ContentPage title='Statistics'>
                    <Dashboard />
                  </ContentPage>
                </Route>

                <Route path='/structures'>
                  <ContentPage title='Structure Management'>
                    <Structures />
                  </ContentPage>
                </Route>

                <Route path='/settings'>
                  <ContentPage title='Settings'>test</ContentPage>
                </Route>
              </Switch>

              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </Router>
      </div>
    );
  }
}
