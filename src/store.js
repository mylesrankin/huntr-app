import appReducer from './reducers';

import thunk from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';

export default (initialState = {}) => {
  return applyMiddleware(thunk)(createStore)(
    appReducer,
    initialState,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  );
};
