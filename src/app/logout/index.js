import React from 'react';

const Logout = (props) => {
  const [loggedOut, setLoggedOut] = React.useState(false);
  React.useEffect(() => {
    props.setSession(null);
    //localStorage.setItem('jwt_login_token', session);
    localStorage.removeItem('jwt_login_token');
    setLoggedOut(true);
  }, [props]);
  return <React.Fragment>{loggedOut ? 'Logged out' : '...'}</React.Fragment>;
};

export default Logout;
