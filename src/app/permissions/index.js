const BASE_REGISTERED = ['registered'];
const RESTRICTED_HUNTER = ['hunter', 'admin'];
const USER_MANAGEMENT = ['admin', 'superadmin'];
const RESTRICTED_STRUCTURES = ['admin', 'industry'];

module.exports = {
  BASE_REGISTERED,
  RESTRICTED_HUNTER,
  USER_MANAGEMENT,
  RESTRICTED_STRUCTURES,
};
