import React, { useEffect } from 'react';
import queryString from 'query-string';
import { withRouter, useHistory } from 'react-router-dom';

import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    '& > * + *': {
      margin: theme.spacing(2),
    },
    margin: theme.spacing(4),
  },
}));

const SsoCallback = (props) => {
  const history = useHistory();
  const classes = useStyles();

  console.log(props);
  const qsValues = queryString.parse(props.location.search);

  useEffect(() => {
    if (qsValues.code) {
      fetch('http://localhost:3003/dev/api/sso/login', {
        method: 'post',
        body: JSON.stringify({ code: qsValues.code }),
        headers: { 'Content-Type': 'application/json' },
      })
        .then((res) => res.json())
        .then((res) => {
          if (res.Authorization) {
            props.setSession(res.Authorization);
            history.push('/');
          }
        })
        .catch((err) => console.error(err));
    }
  }, [history, props, qsValues.code]);
  return (
    <div className={classes.root}>
      <CircularProgress />
    </div>
  );
};

export default withRouter(SsoCallback);
