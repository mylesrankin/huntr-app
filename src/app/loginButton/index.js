import React from 'react';

import eveSSOLoginBadge from './eve-sso-login-black-large.png';

const LoginButton = (props) => {
  let eveBaseUrl = 'login.eveonline.com';
  let clientId = 'e592866ecb3e441e8e30ddb22d48a132';
  let scopes =
    'publicData esi-mail.read_mail.v1 esi-ui.write_waypoint.v1 esi-characters.read_notifications.v1 esi-corporations.read_structures.v1 esi-calendar.read_calendar_events.v1 esi-industry.read_corporation_mining.v1';
  let redirectUri = 'https://localhost:3000/sso-callback';
  let state = 'test123';

  return (
    <a
      href={`https://${eveBaseUrl}/oauth/authorize?response_type=code&redirect_uri=${redirectUri}&client_id=${clientId}&scope=${scopes}&state=${state}`}
    >
      <img src={eveSSOLoginBadge}></img>
    </a>
  );
};

export default LoginButton;
