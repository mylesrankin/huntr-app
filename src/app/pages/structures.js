import React from 'react';

import Acl from '../../components/acl';
import { RESTRICTED_STRUCTURES } from '../permissions';

import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

import CardContent from '../../components/card';
import StructureLists from '../../components/structureLists';
import CurrentTargets from '../../components/currentTargets';
import RecentLocates from '../../components/recentLocates';
import RecentUpdates from '../../components/recentUpdates';

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 300,
  },
}));

const Dashboard = (props) => {
  const classes = useStyles();

  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

  return (
    <React.Fragment>
      <Grid container spacing={3}>
        <Acl permissions={RESTRICTED_STRUCTURES}>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <CardContent>
                <StructureLists></StructureLists>
              </CardContent>
            </Paper>
          </Grid>
        </Acl>
      </Grid>
    </React.Fragment>
  );
};

export default Dashboard;
