import React from 'react';

import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';

import { useLocation } from 'react-router-dom';

import LookupSearch from '../../components/lookupSearch';

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 300,
  },
}));

const Dashboard = (props) => {
  const classes = useStyles();

  let query = useQuery();

  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

  return (
    <React.Fragment>
      <LookupSearch></LookupSearch>
    </React.Fragment>
  );
};

export default Dashboard;
