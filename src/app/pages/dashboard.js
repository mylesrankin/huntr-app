import React from 'react';

import Acl from '../../components/acl';
import { RESTRICTED_HUNTER } from '../permissions';

import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

import CardContent from '../../components/card';
import SubmitIntel from '../../components/submitIntel';
import CurrentTargets from '../../components/currentTargets';
import RecentLocates from '../../components/recentLocates';
import RecentUpdates from '../../components/recentUpdates';

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 300,
  },
}));

const Dashboard = (props) => {
  const classes = useStyles();

  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

  return (
    <React.Fragment>
      <Grid container spacing={3}>
        {/* Chart */}
        <Grid item xs={12} md={4} lg={4}>
          <Paper className={fixedHeightPaper}>
            <CardContent outlined={true} title='Current Targets'>
              <CurrentTargets></CurrentTargets>
            </CardContent>
          </Paper>
        </Grid>
        {/* Recent Deposits */}
        <Grid item xs={12} md={4} lg={4}>
          <Paper className={fixedHeightPaper}>
            <CardContent title='Recent Locates'>
              <RecentLocates></RecentLocates>
            </CardContent>
          </Paper>
        </Grid>
        <Grid item xs={12} md={4} lg={4}>
          <Paper className={fixedHeightPaper}>
            <CardContent title='Recent Updates'>
              <RecentUpdates></RecentUpdates>
            </CardContent>
          </Paper>
        </Grid>
        {/* Recent Orders */}
        <Acl permissions={RESTRICTED_HUNTER}>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <CardContent title='Submit Intel'>
                <SubmitIntel></SubmitIntel>
              </CardContent>
            </Paper>
          </Grid>
        </Acl>
      </Grid>
    </React.Fragment>
  );
};

export default Dashboard;
